"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
var __param = (this && this.__param) || function (paramIndex, decorator) {
    return function (target, key) { decorator(target, key, paramIndex); }
};
var __rest = (this && this.__rest) || function (s, e) {
    var t = {};
    for (var p in s) if (Object.prototype.hasOwnProperty.call(s, p) && e.indexOf(p) < 0)
        t[p] = s[p];
    if (s != null && typeof Object.getOwnPropertySymbols === "function")
        for (var i = 0, p = Object.getOwnPropertySymbols(s); i < p.length; i++) {
            if (e.indexOf(p[i]) < 0 && Object.prototype.propertyIsEnumerable.call(s, p[i]))
                t[p[i]] = s[p[i]];
        }
    return t;
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.UserService = void 0;
const common_1 = require("@nestjs/common");
const typeorm_1 = require("@nestjs/typeorm");
const typeorm_2 = require("typeorm");
const bcrypt = require("bcrypt");
const user_entity_1 = require("./user.entity");
let UserService = class UserService {
    constructor(userRepository) {
        this.userRepository = userRepository;
    }
    async all() {
        return await this.userRepository.find();
    }
    async mailExist(mail) {
        const count = await this.userRepository.count({ mail });
        return count === 1;
    }
    async activate(id) {
        try {
            await this.userRepository.save({ id, isActive: true });
            return true;
        }
        catch (_a) {
            return false;
        }
    }
    async phoneExist(phone) {
        const count = await this.userRepository.count({ phone });
        return count === 1;
    }
    create(input) {
        return this.userRepository.create(input);
    }
    async save(_a) {
        var { password } = _a, input = __rest(_a, ["password"]);
        const salt = await bcrypt.genSalt();
        return await this.userRepository.save(this.create(Object.assign({ password: await bcrypt.hash(password, salt) }, input)));
    }
    async setRefreshToken({ id, refreshToken, }) {
        const salt = await bcrypt.genSalt();
        await this.userRepository.update(id, {
            refreshToken: await bcrypt.hash(refreshToken, salt),
        });
    }
    async removeRefreshToken({ id }) {
        await this.userRepository.update(id, {
            refreshToken: null,
        });
    }
    async getUserIfRefreshTokenMatches({ id, refreshToken, }) {
        const user = await this.findOne(id);
        const isRefreshTokenMatching = await bcrypt.compare(refreshToken, user.refreshToken);
        return isRefreshTokenMatching ? user : null;
    }
    async setPassword(id, newPassword) {
        const user = await this.findOneOrFail(id);
        const salt = await bcrypt.genSalt();
        return await this.userRepository.save(Object.assign(Object.assign({}, user), { password: await bcrypt.hash(newPassword, salt) }));
    }
    async update(id, input) {
        return await this.userRepository.save(Object.assign({ id }, input));
    }
    async findOne(id) {
        return await this.userRepository.findOne(id);
    }
    async findOneOrFail(id) {
        return await this.userRepository.findOneOrFail(id);
    }
    async findOneByMail(mail) {
        return await this.userRepository.findOne({ mail });
    }
    async findOneOrFailByMail(mail) {
        return await this.userRepository.findOneOrFail({ mail });
    }
    async findOneByPhone(phone) {
        return await this.userRepository.findOne({ phone });
    }
    async findOneOrFailByPhone(phone) {
        return await this.userRepository.findOneOrFail({ phone });
    }
};
UserService = __decorate([
    common_1.Injectable(),
    __param(0, typeorm_1.InjectRepository(user_entity_1.User)),
    __metadata("design:paramtypes", [typeorm_2.Repository])
], UserService);
exports.UserService = UserService;
//# sourceMappingURL=user.service.js.map