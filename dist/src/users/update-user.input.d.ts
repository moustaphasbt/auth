import { User } from './user.entity';
declare const UpdateUserInput_base: import("@nestjs/common").Type<Partial<Omit<User, "id" | "refreshToken" | "createdAt" | "updatedAt">>>;
export declare class UpdateUserInput extends UpdateUserInput_base {
}
export {};
