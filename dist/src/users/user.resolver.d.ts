import { User } from './user.entity';
import { UserService } from './user.service';
import { UpdateUserInput } from './dto';
export declare class UserResolver {
    private readonly userService;
    constructor(userService: UserService);
    users(): Promise<User[]>;
    mailExist(mail: string): Promise<boolean>;
    phoneExist(phone: string): Promise<boolean>;
    currentUser(user: User): Promise<User>;
    setPassword({ id }: User, { password }: UpdateUserInput): Promise<{
        password: string;
        id: string;
        gender: import("../..").Genderenum;
        firstName: string;
        lastName: string;
        mail?: string;
        birthday?: Date;
        phone: string;
        isActive: boolean;
        firstLogin: boolean;
        tryNumber: number;
        refreshToken?: string;
        createdAt: Date;
        updatedAt: Date;
    } & User>;
    updateUser({ id }: User, input: UpdateUserInput): Promise<{
        id: string;
        gender?: import("../..").Genderenum;
        firstName?: string;
        lastName?: string;
        mail?: string;
        password?: string;
        birthday?: Date;
        phone?: string;
        isActive?: boolean;
        firstLogin?: boolean;
        tryNumber?: number;
        refreshToken?: string;
        createdAt?: Date;
        updatedAt?: Date;
    } & User>;
    activateUser(id: string): Promise<boolean>;
}
