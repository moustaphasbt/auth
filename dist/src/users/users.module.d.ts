import { DynamicModule } from '@nestjs/common';
import { AuthOptions } from '../options';
export declare class UsersModule {
    static forRoot({ imports }: Pick<AuthOptions, 'imports'>): DynamicModule;
}
