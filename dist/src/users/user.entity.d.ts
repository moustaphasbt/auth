import { Genderenum } from '../utils/common';
export declare class User {
    id: string;
    gender: Genderenum;
    firstName: string;
    lastName: string;
    mail?: string;
    password: string;
    birthday?: Date;
    phone: string;
    isActive: boolean;
    firstLogin: boolean;
    tryNumber: number;
    refreshToken?: string;
    createdAt: Date;
    updatedAt: Date;
}
