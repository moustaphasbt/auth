"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
var __param = (this && this.__param) || function (paramIndex, decorator) {
    return function (target, key) { decorator(target, key, paramIndex); }
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.UserResolver = void 0;
const user_entity_1 = require("./user.entity");
const graphql_1 = require("@nestjs/graphql");
const user_service_1 = require("./user.service");
const dto_1 = require("./dto");
const decorators_1 = require("../decorators");
const metadata_1 = require("../metadata");
let UserResolver = class UserResolver {
    constructor(userService) {
        this.userService = userService;
    }
    async users() {
        return await this.userService.all();
    }
    async mailExist(mail) {
        return this.userService.mailExist(mail);
    }
    async phoneExist(phone) {
        return this.userService.phoneExist(phone);
    }
    async currentUser(user) {
        return user || null;
    }
    async setPassword({ id }, { password }) {
        return this.userService.setPassword(id, password);
    }
    async updateUser({ id }, input) {
        return this.userService.update(id, input);
    }
    async activateUser(id) {
        return this.userService.activate(id);
    }
};
__decorate([
    graphql_1.Query(() => [user_entity_1.User]),
    __metadata("design:type", Function),
    __metadata("design:paramtypes", []),
    __metadata("design:returntype", Promise)
], UserResolver.prototype, "users", null);
__decorate([
    metadata_1.Public(),
    graphql_1.Query(() => Boolean),
    __param(0, graphql_1.Args('mail', { type: () => String })),
    __metadata("design:type", Function),
    __metadata("design:paramtypes", [String]),
    __metadata("design:returntype", Promise)
], UserResolver.prototype, "mailExist", null);
__decorate([
    metadata_1.Public(),
    graphql_1.Query(() => Boolean),
    __param(0, graphql_1.Args('phone', { type: () => String })),
    __metadata("design:type", Function),
    __metadata("design:paramtypes", [String]),
    __metadata("design:returntype", Promise)
], UserResolver.prototype, "phoneExist", null);
__decorate([
    graphql_1.Query(() => user_entity_1.User, { nullable: true }),
    __param(0, decorators_1.CurrentUser()),
    __metadata("design:type", Function),
    __metadata("design:paramtypes", [user_entity_1.User]),
    __metadata("design:returntype", Promise)
], UserResolver.prototype, "currentUser", null);
__decorate([
    graphql_1.Mutation(() => user_entity_1.User),
    __param(0, decorators_1.CurrentUser()),
    __param(1, graphql_1.Args('input', { type: () => dto_1.UpdateUserInput })),
    __metadata("design:type", Function),
    __metadata("design:paramtypes", [user_entity_1.User,
        dto_1.UpdateUserInput]),
    __metadata("design:returntype", Promise)
], UserResolver.prototype, "setPassword", null);
__decorate([
    graphql_1.Mutation(() => user_entity_1.User),
    __param(0, decorators_1.CurrentUser()),
    __param(1, graphql_1.Args('input', { type: () => dto_1.UpdateUserInput })),
    __metadata("design:type", Function),
    __metadata("design:paramtypes", [user_entity_1.User,
        dto_1.UpdateUserInput]),
    __metadata("design:returntype", Promise)
], UserResolver.prototype, "updateUser", null);
__decorate([
    metadata_1.Public(),
    graphql_1.Mutation(() => Boolean),
    __param(0, graphql_1.Args('id', { type: () => String })),
    __metadata("design:type", Function),
    __metadata("design:paramtypes", [String]),
    __metadata("design:returntype", Promise)
], UserResolver.prototype, "activateUser", null);
UserResolver = __decorate([
    metadata_1.Public(),
    graphql_1.Resolver(() => user_entity_1.User),
    __metadata("design:paramtypes", [user_service_1.UserService])
], UserResolver);
exports.UserResolver = UserResolver;
//# sourceMappingURL=user.resolver.js.map