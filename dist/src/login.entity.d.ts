import { User } from './users/user.entity';
export declare class Login {
    id: string;
    ip?: string;
    user: User;
    createdAt: Date;
    updatedAt: Date;
}
