export declare const JWT_OPTIONS = "jwt options";
export interface JwtOptions {
    JWT_ACCESS_TOKEN_SECRET_TIME: number;
    JWT_REFRESH_TOKEN_SECRET_TIME: number;
}
export interface AuthOptions {
    imports: any[];
    useFactory: (config: any) => JwtOptions;
    inject: any[];
}
