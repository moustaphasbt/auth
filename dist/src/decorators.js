"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.ReqGql = exports.ResGql = exports.CurrentUser = void 0;
const common_1 = require("@nestjs/common");
const graphql_1 = require("@nestjs/graphql");
exports.CurrentUser = common_1.createParamDecorator((_, context) => {
    const ctx = graphql_1.GqlExecutionContext.create(context);
    return ctx.getContext().req.user;
});
exports.ResGql = common_1.createParamDecorator((_, context, ...other) => {
    const ctx = graphql_1.GqlExecutionContext.create(context);
    return ctx.getContext().res;
});
exports.ReqGql = common_1.createParamDecorator((_, context) => {
    const ctx = graphql_1.GqlExecutionContext.create(context);
    return ctx.getContext().req;
});
//# sourceMappingURL=decorators.js.map