import { DynamicModule } from '@nestjs/common';
import { AuthOptions } from './options';
export declare class AuthModule {
    static forRoot({ imports, inject, useFactory }: AuthOptions): DynamicModule;
}
