"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
var __param = (this && this.__param) || function (paramIndex, decorator) {
    return function (target, key) { decorator(target, key, paramIndex); }
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.AuthResolver = void 0;
const common_1 = require("@nestjs/common");
const graphql_1 = require("@nestjs/graphql");
const user_entity_1 = require("./users/user.entity");
const auth_service_1 = require("./auth.service");
const user_service_1 = require("./users/user.service");
const decorators_1 = require("./decorators");
const gql_refresh_guard_1 = require("./guard/gql-refresh.guard");
const login_entity_1 = require("./login.entity");
const options_1 = require("./options");
const dto_1 = require("./dto");
const metadata_1 = require("./metadata");
let AuthResolver = class AuthResolver {
    constructor(authService, userService, options) {
        this.authService = authService;
        this.userService = userService;
        this.options = options;
    }
    async login({ username, password }, ip, response) {
        const user = await this.authService.validateUser(username, password);
        const accessToken = await this.authService.accessToken(user.id);
        const refreshToken = await this.authService.refreshToken(user.id);
        await this.userService.setRefreshToken({ id: user.id, refreshToken });
        response.cookie('accessToken', accessToken, {
            path: '/',
            httpOnly: true,
            maxAge: this.options.JWT_ACCESS_TOKEN_SECRET_TIME,
        });
        response.cookie('refreshToken', refreshToken, {
            path: '/',
            httpOnly: true,
            maxAge: this.options.JWT_REFRESH_TOKEN_SECRET_TIME,
        });
        return await this.authService.save({ ip, user });
    }
    async refresh(user, response) {
        const accessToken = this.authService.accessToken(user.id);
        response.cookie('accessToken', accessToken, {
            path: '/',
            httpOnly: true,
            maxAge: this.options.JWT_ACCESS_TOKEN_SECRET_TIME,
        });
        return user;
    }
    async register(input) {
        return this.authService.register(input);
    }
    async logout({ id }, response) {
        try {
            response.cookie('accessToken', '', {
                path: '/',
                httpOnly: true,
            });
            response.cookie('refreshToken', '', {
                path: '/',
                httpOnly: true,
            });
            await this.userService.removeRefreshToken({ id });
            return true;
        }
        catch (_a) {
            return false;
        }
    }
    user({ user }) {
        return user;
    }
};
__decorate([
    metadata_1.Public(),
    graphql_1.Mutation(() => login_entity_1.Login),
    __param(0, graphql_1.Args('input', { type: () => dto_1.LoginInput })),
    __param(1, common_1.Ip()),
    __param(2, decorators_1.ResGql()),
    __metadata("design:type", Function),
    __metadata("design:paramtypes", [dto_1.LoginInput, String, Object]),
    __metadata("design:returntype", Promise)
], AuthResolver.prototype, "login", null);
__decorate([
    common_1.UseGuards(gql_refresh_guard_1.GqlRefreshGuard),
    graphql_1.Query(() => user_entity_1.User),
    __param(0, decorators_1.CurrentUser()),
    __param(1, decorators_1.ResGql()),
    __metadata("design:type", Function),
    __metadata("design:paramtypes", [user_entity_1.User, Object]),
    __metadata("design:returntype", Promise)
], AuthResolver.prototype, "refresh", null);
__decorate([
    metadata_1.Public(),
    graphql_1.Mutation(() => user_entity_1.User),
    __param(0, graphql_1.Args('input', { type: () => dto_1.RegisterInput })),
    __metadata("design:type", Function),
    __metadata("design:paramtypes", [dto_1.RegisterInput]),
    __metadata("design:returntype", Promise)
], AuthResolver.prototype, "register", null);
__decorate([
    graphql_1.Mutation(() => Boolean),
    __param(0, decorators_1.CurrentUser()),
    __param(1, decorators_1.ResGql()),
    __metadata("design:type", Function),
    __metadata("design:paramtypes", [user_entity_1.User, Object]),
    __metadata("design:returntype", Promise)
], AuthResolver.prototype, "logout", null);
__decorate([
    graphql_1.ResolveField(() => user_entity_1.User),
    __param(0, graphql_1.Root()),
    __metadata("design:type", Function),
    __metadata("design:paramtypes", [login_entity_1.Login]),
    __metadata("design:returntype", void 0)
], AuthResolver.prototype, "user", null);
AuthResolver = __decorate([
    metadata_1.Public(),
    graphql_1.Resolver(() => login_entity_1.Login),
    __param(2, common_1.Inject(options_1.JWT_OPTIONS)),
    __metadata("design:paramtypes", [auth_service_1.AuthService,
        user_service_1.UserService, Object])
], AuthResolver);
exports.AuthResolver = AuthResolver;
//# sourceMappingURL=auth.resolver.js.map