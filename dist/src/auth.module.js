"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.AuthModule = void 0;
const passport_1 = require("@nestjs/passport");
const jwt_1 = require("@nestjs/jwt");
const common_1 = require("./utils/common");
const options_1 = require("./options");
const auth_service_1 = require("./auth.service");
const jwt_access_token_strategy_1 = require("./strategy/jwt-access-token.strategy");
const jwt_refresh_token_strategy_1 = require("./strategy/jwt-refresh-token.strategy");
const users_module_1 = require("./users/users.module");
const auth_resolver_1 = require("./auth.resolver");
const gql_refresh_guard_1 = require("./guard/gql-refresh.guard");
class AuthModule {
    static forRoot({ imports, inject, useFactory }) {
        return {
            module: AuthModule,
            imports: [
                ...imports,
                passport_1.PassportModule.register({
                    defaultStrategy: common_1.JWT_ACCESS_TOKEN,
                }),
                jwt_1.JwtModule.register({
                    secret: common_1.JWT_SECRET,
                }),
                users_module_1.UsersModule.forRoot({ imports }),
            ],
            providers: [
                auth_service_1.AuthService,
                auth_resolver_1.AuthResolver,
                gql_refresh_guard_1.GqlRefreshGuard,
                jwt_access_token_strategy_1.JwtAccessTokenStrategy,
                jwt_refresh_token_strategy_1.JwtRefreshTokenStrategy,
                {
                    provide: options_1.JWT_OPTIONS,
                    useFactory,
                    inject,
                },
            ],
            exports: [auth_service_1.AuthService, jwt_access_token_strategy_1.JwtAccessTokenStrategy, jwt_refresh_token_strategy_1.JwtRefreshTokenStrategy],
        };
    }
}
exports.AuthModule = AuthModule;
//# sourceMappingURL=auth.module.js.map