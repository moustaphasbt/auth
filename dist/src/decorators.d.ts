export declare const CurrentUser: (...dataOrPipes: unknown[]) => ParameterDecorator;
export declare const ResGql: (...dataOrPipes: unknown[]) => ParameterDecorator;
export declare const ReqGql: (...dataOrPipes: unknown[]) => ParameterDecorator;
