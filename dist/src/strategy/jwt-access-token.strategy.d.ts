import { Strategy } from 'passport-jwt';
import { Request } from 'express';
import { UserService } from '../users/user.service';
import { User } from '../users/user.entity';
declare const JwtAccessTokenStrategy_base: new (...args: any[]) => Strategy;
export declare class JwtAccessTokenStrategy extends JwtAccessTokenStrategy_base {
    private readonly userService;
    constructor(userService: UserService);
    validate(request: Request, payload: Pick<User, 'id'>): Promise<User>;
}
export {};
