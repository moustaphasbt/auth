import { Request } from 'express';
import { Strategy } from 'passport-jwt';
import { UserService } from '../users/user.service';
import { User } from '../users/user.entity';
declare const JwtRefreshTokenStrategy_base: new (...args: any[]) => Strategy;
export declare class JwtRefreshTokenStrategy extends JwtRefreshTokenStrategy_base {
    private readonly userService;
    constructor(userService: UserService);
    validate(request: Request, payload: Pick<User, 'id'>): Promise<User>;
}
export {};
