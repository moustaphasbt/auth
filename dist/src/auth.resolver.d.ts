import { User } from './users/user.entity';
import { AuthService } from './auth.service';
import { UserService } from './users/user.service';
import { Response } from 'express';
import { Login } from './login.entity';
import { JwtOptions } from './options';
import { LoginInput, RegisterInput } from './dto';
export declare class AuthResolver {
    private authService;
    private userService;
    private options;
    constructor(authService: AuthService, userService: UserService, options: JwtOptions);
    login({ username, password }: LoginInput, ip: string, response: Response): Promise<Login>;
    refresh(user: User, response: Response): Promise<User>;
    register(input: RegisterInput): Promise<User>;
    logout({ id }: User, response: Response): Promise<boolean>;
    user({ user }: Login): User;
}
