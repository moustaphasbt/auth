import { ExecutionContext } from '@nestjs/common';
declare const GqlRefreshGuard_base: import("@nestjs/passport").Type<import("@nestjs/passport").IAuthGuard>;
export declare class GqlRefreshGuard extends GqlRefreshGuard_base {
    getRequest(context: ExecutionContext): any;
}
export {};
