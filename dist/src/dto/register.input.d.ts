import { User } from '../users/user.entity';
declare const RegisterInput_base: import("@nestjs/common").Type<Pick<User, "gender" | "firstName" | "lastName" | "mail" | "password" | "phone">>;
export declare class RegisterInput extends RegisterInput_base {
}
export {};
