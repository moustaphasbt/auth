"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.Genderenum = exports.IS_PUBLIC_KEY = exports.JWT_SECRET = exports.JWT_REFRESH_TOKEN = exports.JWT_ACCESS_TOKEN = void 0;
exports.JWT_ACCESS_TOKEN = 'jwt-access-token';
exports.JWT_REFRESH_TOKEN = 'jwt-refresh-token';
exports.JWT_SECRET = 'sant-yalla-rek';
exports.IS_PUBLIC_KEY = 'isPublic';
var Genderenum;
(function (Genderenum) {
    Genderenum["MASCULIN"] = "masculin";
    Genderenum["FEMININ"] = "feminin";
})(Genderenum = exports.Genderenum || (exports.Genderenum = {}));
//# sourceMappingURL=common.js.map