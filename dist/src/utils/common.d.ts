export declare const JWT_ACCESS_TOKEN = "jwt-access-token";
export declare const JWT_REFRESH_TOKEN = "jwt-refresh-token";
export declare const JWT_SECRET = "sant-yalla-rek";
export declare const IS_PUBLIC_KEY = "isPublic";
export declare enum Genderenum {
    MASCULIN = "masculin",
    FEMININ = "feminin"
}
