import { JwtService } from '@nestjs/jwt';
import { UserService } from './users/user.service';
import { Repository } from 'typeorm';
import { Login } from './login.entity';
import { RegisterInput } from './dto';
export declare class AuthService {
    private readonly jwtService;
    private readonly userService;
    private loginRepository;
    constructor(jwtService: JwtService, userService: UserService, loginRepository: Repository<Login>);
    validateUser(username: string, password: string): Promise<import("..").User>;
    findOrFailLogin(id: string): Promise<Login>;
    create({ ip, user }: Pick<Login, 'ip' | 'user'>): Login;
    save({ ip, user }: Pick<Login, 'ip' | 'user'>): Promise<Login>;
    register(input: RegisterInput): Promise<import("..").User>;
    accessToken(id: string): Promise<string>;
    refreshToken(id: string): Promise<string>;
}
