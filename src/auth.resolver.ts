import { Inject, Ip, UseGuards } from '@nestjs/common';
import {
  Args,
  Mutation,
  Query,
  ResolveField,
  Resolver,
  Root,
} from '@nestjs/graphql';
import { User } from './users/user.entity';
import { AuthService } from './auth.service';
import { UserService } from './users/user.service';
import { CurrentUser, ResGql } from './decorators';
import { Response } from 'express';
import { GqlRefreshGuard } from './guard/gql-refresh.guard';
import { Login } from './login.entity';
import { JwtOptions, JWT_OPTIONS } from './options';
import { LoginInput, RegisterInput } from './dto';
import { Public } from './metadata';

@Public()
@Resolver(() => Login)
export class AuthResolver {
  constructor(
    private authService: AuthService,
    private userService: UserService,
    @Inject(JWT_OPTIONS)
    private options: JwtOptions,
  ) {}

  @Public()
  @Mutation(() => Login)
  async login(
    @Args('input', { type: () => LoginInput })
    { username, password }: LoginInput,
    @Ip() ip: string,
    @ResGql() response: Response,
  ) {
    const user = await this.authService.validateUser(username, password);
    const accessToken = await this.authService.accessToken(user.id);
    const refreshToken = await this.authService.refreshToken(user.id);
    await this.userService.setRefreshToken({ id: user.id, refreshToken });
    response.cookie('accessToken', accessToken, {
      path: '/',
      httpOnly: true,
      maxAge: this.options.JWT_ACCESS_TOKEN_SECRET_TIME,
    });
    response.cookie('refreshToken', refreshToken, {
      path: '/',
      httpOnly: true,
      maxAge: this.options.JWT_REFRESH_TOKEN_SECRET_TIME,
    });

    return await this.authService.save({ ip, user });
  }

  @UseGuards(GqlRefreshGuard)
  @Query(() => User)
  async refresh(@CurrentUser() user: User, @ResGql() response: Response) {
    const accessToken = this.authService.accessToken(user.id);
    response.cookie('accessToken', accessToken, {
      path: '/',
      httpOnly: true,
      maxAge: this.options.JWT_ACCESS_TOKEN_SECRET_TIME,
    });
    return user;
  }

  @Public()
  @Mutation(() => User)
  async register(
    @Args('input', { type: () => RegisterInput }) input: RegisterInput,
  ) {
    return this.authService.register(input);
  }

  @Mutation(() => Boolean)
  async logout(@CurrentUser() { id }: User, @ResGql() response: Response) {
    try {
      response.cookie('accessToken', '', {
        path: '/',
        httpOnly: true,
      });
      response.cookie('refreshToken', '', {
        path: '/',
        httpOnly: true,
      });
      await this.userService.removeRefreshToken({ id });
      return true;
    } catch {
      return false;
    }
  }

  @ResolveField(() => User)
  user(@Root() { user }: Login) {
    return user;
  }
}
