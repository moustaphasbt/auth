import {
  Column,
  CreateDateColumn,
  Entity,
  PrimaryGeneratedColumn,
  UpdateDateColumn,
} from 'typeorm';
import { Field, ObjectType, registerEnumType } from '@nestjs/graphql';
import { Genderenum } from '../utils/common';
import { IsBoolean, IsDate, IsEmail, IsNumber, IsUUID } from 'class-validator';

registerEnumType(Genderenum, {
  name: 'Genderenum',
});

@ObjectType()
@Entity()
export class User {
  @IsUUID()
  @Field(() => String)
  @PrimaryGeneratedColumn('uuid')
  id: string;

  @Field(() => Genderenum)
  @Column('enum', { enum: Genderenum })
  gender: Genderenum;

  @Field(() => String)
  @Column()
  firstName: string;

  @Field(() => String)
  @Column()
  lastName: string;

  @Field(() => String, { nullable: true })
  @Column({ unique: true })
  @IsEmail()
  mail?: string;

  @Field()
  @Column()
  password: string;

  @IsDate()
  @Field({ nullable: true })
  @Column('date', { nullable: true })
  birthday?: Date;

  @Field(() => String)
  @Column({ unique: true })
  phone: string;

  @IsBoolean()
  @Field(() => Boolean)
  @Column('boolean', { default: false })
  isActive: boolean;

  @IsBoolean()
  @Field(() => Boolean)
  @Column('boolean', { default: true })
  firstLogin: boolean;

  @IsNumber()
  @Field(() => Number)
  @Column('int', { default: 0 })
  tryNumber: number;

  @Column({ nullable: true })
  refreshToken?: string;

  @Field(() => Date)
  @CreateDateColumn()
  createdAt: Date;

  @Field(() => Date)
  @UpdateDateColumn()
  updatedAt: Date;
}
