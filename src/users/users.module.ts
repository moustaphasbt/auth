import { DynamicModule, Module } from '@nestjs/common';
import { AuthOptions } from '../options';
import { UserResolver } from './user.resolver';
import { UserService } from './user.service';

@Module({})
export class UsersModule {
  static forRoot({ imports }: Pick<AuthOptions, 'imports'>): DynamicModule {
    return {
      module: UsersModule,
      imports,
      providers: [UserService, UserResolver],
      exports: [UserService],
    };
  }
}
