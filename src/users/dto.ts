import { Field, InputType } from '@nestjs/graphql';
import { IsDate, IsEmail, IsNotEmpty, IsUUID } from 'class-validator';
import { Genderenum } from '../utils/common';

@InputType()
export class UserIdInput {
  @IsNotEmpty()
  @IsUUID()
  @Field(() => String)
  id: string;
}

@InputType()
export class UpdateUserInput {
  @Field(() => Genderenum, { nullable: true })
  gender?: Genderenum;

  @Field(() => String, { nullable: true })
  firstName?: string;

  @Field(() => String, { nullable: true })
  lastName?: string;

  @IsEmail()
  @Field(() => String, { nullable: true })
  mail?: string;

  @Field(() => String, { nullable: true })
  password?: string;

  @IsDate()
  @Field(() => Date, { nullable: true })
  birthday?: Date;

  @Field(() => String, { nullable: true })
  phone?: string;
}
