import { Injectable } from '@nestjs/common';
import { InjectRepository } from '@nestjs/typeorm';
import { Repository } from 'typeorm';
import * as bcrypt from 'bcrypt';
import { User } from './user.entity';

@Injectable()
export class UserService {
  constructor(
    @InjectRepository(User)
    private userRepository: Repository<User>,
  ) {}

  async all() {
    return await this.userRepository.find();
  }

  async mailExist(mail: string) {
    const count = await this.userRepository.count({ mail });
    return count === 1;
  }

  async activate(id: string) {
    try {
      await this.userRepository.save({ id, isActive: true });
      return true;
    } catch {
      return false;
    }
  }

  async phoneExist(phone: string) {
    const count = await this.userRepository.count({ phone });
    return count === 1;
  }

  create(input: Partial<User>) {
    return this.userRepository.create(input);
  }

  async save({ password, ...input }: Partial<User>) {
    const salt = await bcrypt.genSalt();
    return await this.userRepository.save(
      this.create({
        password: await bcrypt.hash(password, salt),
        ...input,
      }),
    );
  }

  async setRefreshToken({
    id,
    refreshToken,
  }: Pick<User, 'id' | 'refreshToken'>) {
    const salt = await bcrypt.genSalt();
    await this.userRepository.update(id, {
      refreshToken: await bcrypt.hash(refreshToken, salt),
    });
  }

  async removeRefreshToken({ id }: Pick<User, 'id'>) {
    await this.userRepository.update(id, {
      refreshToken: null,
    });
  }

  async getUserIfRefreshTokenMatches({
    id,
    refreshToken,
  }: Pick<User, 'id' | 'refreshToken'>) {
    const user = await this.findOne(id);
    const isRefreshTokenMatching = await bcrypt.compare(
      refreshToken,
      user.refreshToken,
    );
    return isRefreshTokenMatching ? user : null;
  }

  async setPassword(id: string, newPassword: string) {
    const user = await this.findOneOrFail(id);
    const salt = await bcrypt.genSalt();
    return await this.userRepository.save({
      ...user,
      password: await bcrypt.hash(newPassword, salt),
    });
  }

  async update(id: string, input: Partial<User>) {
    return await this.userRepository.save({ id, ...input });
  }

  async findOne(id: string) {
    return await this.userRepository.findOne(id);
  }

  async findOneOrFail(id: string) {
    return await this.userRepository.findOneOrFail(id);
  }

  async findOneByMail(mail: string) {
    return await this.userRepository.findOne({ mail });
  }

  async findOneOrFailByMail(mail: string) {
    return await this.userRepository.findOneOrFail({ mail });
  }

  async findOneByPhone(phone: string) {
    return await this.userRepository.findOne({ phone });
  }

  async findOneOrFailByPhone(phone: string) {
    return await this.userRepository.findOneOrFail({ phone });
  }
}
