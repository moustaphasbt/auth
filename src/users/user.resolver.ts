import { User } from './user.entity';
import { Resolver, Query, Mutation, Args } from '@nestjs/graphql';
import { UserService } from './user.service';
import { UpdateUserInput } from './dto';
import { CurrentUser } from '../decorators';
import { Public } from '../metadata';

@Public()
@Resolver(() => User)
export class UserResolver {
  constructor(private readonly userService: UserService) {}

  @Query(() => [User])
  async users() {
    return await this.userService.all();
  }

  @Public()
  @Query(() => Boolean)
  async mailExist(@Args('mail', { type: () => String }) mail: string) {
    return this.userService.mailExist(mail);
  }

  @Public()
  @Query(() => Boolean)
  async phoneExist(@Args('phone', { type: () => String }) phone: string) {
    return this.userService.phoneExist(phone);
  }

  @Query(() => User, { nullable: true })
  async currentUser(@CurrentUser() user: User) {
    return user || null;
  }

  @Mutation(() => User)
  async setPassword(
    @CurrentUser() { id }: User,
    @Args('input', { type: () => UpdateUserInput })
    { password }: UpdateUserInput,
  ) {
    return this.userService.setPassword(id, password);
  }

  @Mutation(() => User)
  async updateUser(
    @CurrentUser() { id }: User,
    @Args('input', { type: () => UpdateUserInput }) input: UpdateUserInput,
  ) {
    return this.userService.update(id, input);
  }

  @Public()
  @Mutation(() => Boolean)
  async activateUser(@Args('id', { type: () => String }) id: string) {
    return this.userService.activate(id);
  }
}
