import { Request } from 'express';
import { ExtractJwt, Strategy } from 'passport-jwt';
import { PassportStrategy } from '@nestjs/passport';
import { Injectable } from '@nestjs/common';
import { UserService } from '../users/user.service';
import { JWT_REFRESH_TOKEN, JWT_SECRET } from '../utils/common';
import { User } from '../users/user.entity';

@Injectable()
export class JwtRefreshTokenStrategy extends PassportStrategy(
  Strategy,
  JWT_REFRESH_TOKEN,
) {
  constructor(private readonly userService: UserService) {
    super({
      jwtFromRequest: ExtractJwt.fromExtractors([
        (request: Request) => {
          return request?.cookies?.refreshToken;
        },
      ]),
      secretOrKey: JWT_SECRET,
      passReqToCallback: true,
    });
  }

  async validate(request: Request, payload: Pick<User, 'id'>) {
    const refreshToken = request.cookies?.refreshToken;
    return this.userService.getUserIfRefreshTokenMatches({
      id: payload.id,
      refreshToken,
    });
  }
}
