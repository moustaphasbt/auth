import { ExtractJwt, Strategy } from 'passport-jwt';
import { PassportStrategy } from '@nestjs/passport';
import { Injectable } from '@nestjs/common';
import { Request } from 'express';
import { UserService } from '../users/user.service';
import { JWT_ACCESS_TOKEN, JWT_SECRET } from '../utils/common';
import { User } from '../users/user.entity';

@Injectable()
export class JwtAccessTokenStrategy extends PassportStrategy(
  Strategy,
  JWT_ACCESS_TOKEN,
) {
  constructor(private readonly userService: UserService) {
    super({
      jwtFromRequest: ExtractJwt.fromExtractors([
        (request: Request) => {
          return request?.cookies?.accessToken;
        },
      ]),
      secretOrKey: JWT_SECRET,
      passReqToCallback: true,
    });
  }

  async validate(request: Request, payload: Pick<User, 'id'>) {
    const refreshToken = request.cookies?.accessToken;
    return this.userService.getUserIfRefreshTokenMatches({
      id: payload.id,
      refreshToken,
    });
  }
}
