import { DynamicModule } from '@nestjs/common';
import { PassportModule } from '@nestjs/passport';
import { JwtModule } from '@nestjs/jwt';
import { JWT_ACCESS_TOKEN, JWT_SECRET } from './utils/common';
import { AuthOptions, JWT_OPTIONS } from './options';
import { AuthService } from './auth.service';
import { JwtAccessTokenStrategy } from './strategy/jwt-access-token.strategy';
import { JwtRefreshTokenStrategy } from './strategy/jwt-refresh-token.strategy';
import { UsersModule } from './users/users.module';
import { AuthResolver } from './auth.resolver';
import { GqlRefreshGuard } from './guard/gql-refresh.guard';

export class AuthModule {
  static forRoot({ imports, inject, useFactory }: AuthOptions): DynamicModule {
    return {
      module: AuthModule,
      imports: [
        ...imports,
        PassportModule.register({
          defaultStrategy: JWT_ACCESS_TOKEN,
        }),
        JwtModule.register({
          secret: JWT_SECRET,
        }),
        UsersModule.forRoot({ imports }),
      ],
      providers: [
        AuthService,
        AuthResolver,
        GqlRefreshGuard,
        JwtAccessTokenStrategy,
        JwtRefreshTokenStrategy,
        {
          provide: JWT_OPTIONS,
          useFactory,
          inject,
        },
      ],
      exports: [AuthService, JwtAccessTokenStrategy, JwtRefreshTokenStrategy],
    };
  }
}
