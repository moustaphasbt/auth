import { Injectable, UnauthorizedException } from '@nestjs/common';
import { JwtService } from '@nestjs/jwt';
import { UserService } from './users/user.service';
import * as bcrypt from 'bcrypt';
import { Repository } from 'typeorm';
import { Login } from './login.entity';
import { InjectRepository } from '@nestjs/typeorm';
import { JWT_SECRET } from './utils/common';
import isEmail from 'validator/lib/isEmail';
import { RegisterInput } from './dto';

@Injectable()
export class AuthService {
  constructor(
    private readonly jwtService: JwtService,
    private readonly userService: UserService,
    @InjectRepository(Login)
    private loginRepository: Repository<Login>,
  ) {}

  async validateUser(username: string, password: string) {
    const user = await (isEmail(username)
      ? this.userService.findOneOrFailByMail(username)
      : this.userService.findOneOrFailByPhone(username));
    if (!user) {
      throw new UnauthorizedException(`${username}  not found`);
    }
    const isMatch = await bcrypt.compare(password, user.password);
    if (!isMatch) {
      throw new UnauthorizedException('incorrect password');
    }
    return user;
  }

  async findOrFailLogin(id: string) {
    return await this.loginRepository.findOneOrFail(id);
  }

  create({ ip, user }: Pick<Login, 'ip' | 'user'>) {
    return this.loginRepository.create({ ip, user });
  }

  async save({ ip, user }: Pick<Login, 'ip' | 'user'>) {
    return await this.loginRepository.save(
      this.create({
        ip,
        user,
      }),
    );
  }

  async register(input: RegisterInput) {
    return await this.userService.save(input);
  }

  async accessToken(id: string) {
    const payload = { id };
    return this.jwtService.sign(payload, {
      secret: JWT_SECRET,
    });
  }

  async refreshToken(id: string) {
    const payload = { id };
    return this.jwtService.sign(payload, {
      secret: JWT_SECRET,
    });
  }
}
