import { ExecutionContext, Injectable } from '@nestjs/common';
import { GqlExecutionContext } from '@nestjs/graphql';
import { AuthGuard } from '@nestjs/passport';
import { JWT_REFRESH_TOKEN } from '../utils/common';

@Injectable()
export class GqlRefreshGuard extends AuthGuard(JWT_REFRESH_TOKEN) {
  getRequest(context: ExecutionContext) {
    const ctx = GqlExecutionContext.create(context);
    return ctx.getContext().req;
  }
}
