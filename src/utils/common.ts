export const JWT_ACCESS_TOKEN = 'jwt-access-token';
export const JWT_REFRESH_TOKEN = 'jwt-refresh-token';
export const JWT_SECRET = 'sant-yalla-rek';
export const IS_PUBLIC_KEY = 'isPublic';

export enum Genderenum {
  MASCULIN = 'masculin',
  FEMININ = 'feminin',
}
