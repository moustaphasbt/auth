import { Field, InputType } from '@nestjs/graphql';
import { IsEmail, IsNotEmpty, IsUUID } from 'class-validator';
import { Genderenum } from './utils/common';

@InputType()
export class LoginIdInput {
  @IsNotEmpty()
  @IsUUID()
  @Field(() => String)
  id: string;
}

@InputType()
export class LoginInput {
  @IsNotEmpty()
  @Field()
  username: string;

  @IsNotEmpty()
  @Field()
  password: string;
}

@InputType()
export class RegisterInput {
  @Field(() => Genderenum)
  gender: Genderenum;

  @IsNotEmpty()
  @Field(() => String)
  firstName: string;

  @IsNotEmpty()
  @Field(() => String)
  lastName: string;

  @IsEmail()
  @Field(() => String, { nullable: true })
  mail?: string;

  @IsNotEmpty()
  @Field(() => String)
  password: string;

  @IsNotEmpty()
  @Field(() => String)
  phone: string;
}
